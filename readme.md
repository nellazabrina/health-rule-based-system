# Health Rule Based System

## Develop and Run

### Requirement
1. Docker
```bash
$ sudo apt install docker.io docker docker-compose
```

### Setting Up
Pull the images in docker repo
```bash
$ sudo docker pull cupercupu/clipspy
```

### Run the App
```bash
$ sudo docker-compose up
```

## Deployment
Using heroku

### Install heroku cli
```bash
$ sudo snap install heroku --classic
```

### Deploy docker image

Login to heroku
```bash
$ heroku login
$ heroku container:login
```

Push docker image and release
```bash
$ heroku container:push web -a health-rbs
$ heroku container:release web -a health-rbs
```

## Documentation
Find documentation of Health Diagnose:
1. http://eprints.ums.ac.id/44594/6/NASKAH%20PUBLIKASI_L200120080.pdf (Schizophrenia Decision Tree)

Find documentation of clipspy:
1. https://pypi.org/project/clipspy/
2. https://clipspy.readthedocs.io/en/latest/clips.html
3. https://kcir.pwr.edu.pl/~witold/ai/CLIPS_tutorial/CLIPS_tutorial_1.html (tutorial 1-6)