(defrule check-complicated1
   (gangguan G01)
   (gangguan G02)
   =>
   (assert (gangguan C01))
)

(defrule check-complicated2
   (gangguan G01)
   (not (gangguan G02))
   =>
   (assert (gangguan C02))
)

(defrule check-complicated3
   (gangguan C01)
   (gangguan G03)
   =>
   (assert (gangguan C03))
)

(defrule check-complicated4
   (gangguan C01)
   (not (gangguan G03))
   =>
   (assert (gangguan C04))
)

(defrule check-complicated5
   (gangguan C02)
   (gangguan G03)
   =>
   (assert (gangguan C05))
)

(defrule check-complicated6
   (gangguan C02)
   (not (gangguan G03))
   =>
   (assert (gangguan C06))
)

(defrule check-complicated7
   (gangguan C03)
   (gangguan G04)
   =>
   (assert (gangguan C07))
)

(defrule check-complicated8
   (gangguan C03)
   (not (gangguan G04))
   =>
   (assert (gangguan C08))
)

(defrule check-complicated9
   (gangguan C04)
   (gangguan G04)
   =>
   (assert (gangguan C09))
)

(defrule check-complicated10
   (gangguan C04)
   (not (gangguan G04))
   =>
   (assert (gangguan C10))
)

(defrule check-complicated11
   (gangguan C05)
   (gangguan G04)
   =>
   (assert (gangguan C11))
)

(defrule check-complicated12
   (gangguan C05)
   (not (gangguan G04))
   =>
   (assert (gangguan C12))
)

(defrule check-complicated13
   (gangguan C06)
   (gangguan G04)
   =>
   (assert (gangguan C13))
)

(defrule check-complicated14
   (gangguan C06)
   (not (gangguan G04))
   =>
   (assert (gangguan C14))
)

(defrule check-complicated15
   (not (gangguan G01))
   (gangguan G02)
   =>
   (assert (gangguan C15))
)

(defrule check-complicated16
   (not (gangguan G01))
   (not (gangguan G02))
   =>
   (assert (gangguan C16))
)

(defrule check-complicated17
   (gangguan C15)
   (gangguan G03)
   =>
   (assert (gangguan C17))
)

(defrule check-complicated18
   (gangguan C15)
   (not (gangguan G03))
   =>
   (assert (gangguan C18))
)

(defrule check-complicated19
   (gangguan C16)
   (gangguan G03)
   =>
   (assert (gangguan C19))
)

(defrule check-complicated20
   (gangguan C16)
   (not (gangguan G03))
   =>
   (assert (gangguan C20))
)

(defrule check-complicated21
   (gangguan C17)
   (gangguan G04)
   =>
   (assert (gangguan C21))
)

(defrule check-complicated22
   (gangguan C17)
   (not (gangguan G04))
   =>
   (assert (gangguan C22))
)

(defrule check-complicated23
   (gangguan C18)
   (gangguan G04)
   =>
   (assert (gangguan C23))
)

(defrule check-complicated24
   (gangguan C18)
   (not (gangguan G04))
   =>
   (assert (gangguan C24))
)

(defrule check-complicated25
   (gangguan C19)
   (gangguan G04)
   =>
   (assert (gangguan C25))
)

(defrule check-complicated26
   (gangguan C19)
   (not (gangguan G04))
   =>
   (assert (gangguan C26))
)

(defrule check-complicated27
   (gangguan C20)
   (gangguan G06)
   =>
   (assert (gangguan C27))
)

(defrule check-complicated28
   (gangguan C20)
   (not (gangguan G06))
   =>
   (assert (gangguan C28))
)

(defrule check-penyakit1-1
   (gangguan C08)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-2
   (gangguan C08)
   (not (gangguan G05))
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-3
   (gangguan C07)
   (not (gangguan G05))
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-4
   (gangguan C07)
   (gangguan G05)
   (not (gangguan G06))
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit4-1
   (gangguan C07)
   (gangguan G05)
   (gangguan G06)
   (gangguan G07)
   (gangguan G08)
   (gangguan G09)
   (gangguan G10)
   (gangguan G11)
   =>
   (assert (penyakit P04))
)

(defrule check-penyakit1-5
   (gangguan C09)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-6
   (gangguan C09)
   (not (gangguan G05))
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-7
   (gangguan C10)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-8
   (gangguan C11)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-9
   (gangguan C11)
   (not (gangguan G05))
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-10
   (gangguan C12)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-11
   (gangguan C13)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit3-1
   (gangguan C14)
   (gangguan G08)
   (gangguan G09)
   (gangguan G10)
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-2
   (gangguan C14)
   (gangguan G08)
   (gangguan G09)
   (not (gangguan G10))
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-3
   (gangguan C14)
   (gangguan G08)
   (gangguan G09)
   (not (gangguan G10))
   (not (gangguan G11))
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-4
   (gangguan C14)
   (gangguan G08)
   (not (gangguan G09))
   (gangguan G10)
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-5
   (gangguan C14)
   (gangguan G08)
   (not (gangguan G09))
   (gangguan G10)
   (not (gangguan G11))
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-6
   (gangguan C14)
   (gangguan G08)
   (not (gangguan G09))
   (not (gangguan G10))
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-7
   (gangguan C14)
   (not (gangguan G08))
   (gangguan G09)
   (gangguan G10)
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-8
   (gangguan C14)
   (not (gangguan G08))
   (gangguan G09)
   (gangguan G10)
   (not (gangguan G11))
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-9
   (gangguan C14)
   (not (gangguan G08))
   (gangguan G09)
   (not (gangguan G10))
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-10
   (gangguan C14)
   (not (gangguan G08))
   (not (gangguan G09))
   (gangguan G10)
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit1-12
   (gangguan C21)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-13
   (gangguan C21)
   (not (gangguan G05))
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-14
   (gangguan C22)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-15
   (gangguan C23)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit1-16
   (gangguan C25)
   (gangguan G05)
   =>
   (assert (penyakit P01))
)

(defrule check-penyakit2-1
   (gangguan C27)
   (gangguan G07)
   (gangguan G08)
   (gangguan G09)
   =>
   (assert (penyakit P02))
)

(defrule check-penyakit2-2
   (gangguan C27)
   (gangguan G07)
   (gangguan G08)
   (not (gangguan G09))
   =>
   (assert (penyakit P02))
)

(defrule check-penyakit2-3
   (gangguan C27)
   (gangguan G07)
   (not (gangguan G08))
   (gangguan G09)
   =>
   (assert (penyakit P02))
)

(defrule check-penyakit2-4
   (gangguan C27)
   (not (gangguan G07))
   (gangguan G08)
   (gangguan G09)
   =>
   (assert (penyakit P02))
)

(defrule check-penyakit2-5
   (gangguan C28)
   (gangguan G07)
   (gangguan G08)
   (gangguan G09)
   =>
   (assert (penyakit P02))
)

(defrule check-penyakit3-11
   (gangguan C28)
   (not (gangguan G07))
   (gangguan G08)
   (gangguan G09)
   (gangguan G10)
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-12
   (gangguan C28)
   (not (gangguan G07))
   (gangguan G08)
   (gangguan G09)
   (gangguan G10)
   (not (gangguan G11))
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-13
   (gangguan C28)
   (not (gangguan G07))
   (gangguan G08)
   (gangguan G09)
   (not (gangguan G10))
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-14
   (gangguan C28)
   (not (gangguan G07))
   (gangguan G08)
   (not (gangguan G09))
   (gangguan G10)
   (gangguan G11)
   =>
   (assert (penyakit P03))
)

(defrule check-penyakit3-15
   (gangguan C28)
   (not (gangguan G07))
   (not (gangguan G08))
   (gangguan G09)
   (gangguan G10)
   (gangguan G11)
   =>
   (assert (penyakit P03))
)