import os

from flask import Flask, render_template, request
import health_rbs as rbs

app = Flask(__name__, template_folder="templates")
app.jinja_env.cache = None

symptoms_list = [x for x in rbs.labels.items() if x[0].startswith('G')]
symptoms_codes = [x[0] for x in symptoms_list]

@app.route("/", methods=['GET', 'POST'])
def home():
    healthy = None
    disease = None
    if request.method == 'POST':
        checked = []
        symptoms = []
        for code in symptoms_codes:
            if code in request.form:
                symptoms.append(code)
                checked.append(True)
                continue
            checked.append(False)
        healthy, disease = rbs.predict_disease(symptoms)
        options = [(*x, 'checked' if c else '') for x, c in zip(symptoms_list, checked)]
    else:
        options = [(*x, '') for x in symptoms_list]

    return render_template(
        "index.html",
        symptoms=options,
        status=healthy,
        result=disease
    )

def main():
    port = int(os.environ.get('PORT', 8080))

    app.run(debug=True, port=port, host='0.0.0.0')

if __name__ == "__main__":
    main()