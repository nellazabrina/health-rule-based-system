from clipsq import System
import json

rbs = System('rules/schizophrenia.clp')
with open('rules/labels.json') as f:
    labels = json.load(f)

def parse_result(facts):
    if facts:
        facts = list(filter(lambda x: x[0] == 'penyakit', facts))
        if facts:
            last = facts[0][1]
            return last
    return None

def predict_disease(inputs):
    symptoms = []
    for input in inputs:
        temp = "gangguan " + input
        symptoms.append(temp)

    if len(symptoms) == 0:
        penyakit = None
        isHealthy = True
    else: 
        isHealthy = False
        penyakit = rbs.query.inserts(symptoms).execute()
        penyakit = parse_result(penyakit)
        if penyakit is None:
            penyakit = 'unknown'
        else:
            penyakit = labels[penyakit]

    return isHealthy, penyakit