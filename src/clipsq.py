from __future__ import annotations
from multiprocessing import Pipe, Process
import sys

from clips import Environment

def parse_fact(fact):
    if isinstance(fact, list) or isinstance(fact, tuple) or isinstance(fact, set):
        fact = ' '.join([str(f) for f in fact])
    elif isinstance(fact, dict):
        raise TypeError("Can't parse a dictionary type")
    return str(fact)

class FactContainer:
    def __init__(self):
        self._facts = []

    def inserts(self, facts):
        if isinstance(facts, list) or isinstance(facts, tuple) or isinstance(facts, set):
            for fact in facts:
                self.insert(fact)
        elif isinstance(facts, dict):
            for name, value in facts.items():
                self.insert(name + ' ' + parse_fact(value))
        else:
            self.insert(facts)

    def insert(self, fact):
        self._facts.append(parse_fact(fact))

    def inject(self, env):
        for fact in self._facts:
            env.assert_string(f'({fact})')

    def __iter__(self):
        return iter(self._facts)

class ContainsFactTrait:

    def __init__(self):
        self._facts = FactContainer()

    def inserts(self, facts) -> Query:
        self._facts.inserts(facts)
        return self

    def insert(self, fact) -> Query:
        self._facts.insert(fact)
        return self

class System(ContainsFactTrait):

    def __init__(self, filename):
        super().__init__()
        self.filename = filename
        self.env = Environment()
        self.env.load(self.filename)
        
    @property
    def query(self) -> Query:
        self.env.reset()
        return Query(self.env, self._facts)

class Query(ContainsFactTrait):

    def __init__(self, env, facts):
        super().__init__()
        self.env = env
        self._facts._facts.extend(facts._facts)

    def execute(self):
        parent_conn, child_conn = Pipe()
        def call():
            child_conn.send(self._execute())
            child_conn.close()
        p = Process(target=call)
        p.start()
        result = parent_conn.recv()
        p.join()
        return result

    def _execute(self):
        self._facts.inject(self.env)
        self.env.run()
        return Result(self.env.facts)

class Result(list):

    def __init__(self, facts_func):
        for f in facts_func():
            value = str(f)
            try:
                idx = value.index(' ')
                name = value[1:idx]
            except ValueError:
                name = value[1:-1]
            values = tuple(f)
            self.append((name, *values))

