FROM cupercupu/clipspy

WORKDIR /app
ADD requirements.txt requirements.txt

RUN pip install -r requirements.txt

ADD rules rules
ADD src src

CMD PYTHONPATH=src gunicorn main:app -b 0.0.0.0:${PORT:-8080}
